
const add = (x, y, callback) => {
    setTimeout(() => {
        const z = x + y
        callback(z)
    }, 2000)
}

add(1, 4, (sum) => {
    console.log('1st ' + sum)
})

const sub = (x, y, callback) => {
    callback(x - y)
}

sub(9, 3, (subtract) => {
    console.log('2nd ' + subtract)
})