//object property shorthand
const name = 'ketul'
const userAge = 34

const user = {
    name, //you have used here variable+value in one go
    age: userAge,
    location: 'Sidhpur'
}

console.log(user)

//object destructuring
const product = {
    label: 'Red Notebook',
    cost: 30,
    stock: 201,
    MRP: undefined,
    rating: 4.5
}

const { label: productLabel, stock, rating = 5 } = product

console.log(productLabel) //used different variable name
console.log(stock)
console.log(rating) //set default value. If found will use that else set default value

//destructuring in function argument
const transation = (type, { label, stock }) => { //here we have passed destructuring variables in argument itself
    console.log(type, label, stock)
}

transation('order', product) //called function & passed object 'product'