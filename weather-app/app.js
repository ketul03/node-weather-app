const geocode = require('./utils/geocode.js')
const forecast = require('./utils/forecast.js')

const location = process.argv[2]

if (!location) {
    console.log('please enter the location as argument')
} else {
    geocode.geocode(location, (error, { latitude, longitude, location } = {}) => {
        if (error) {
            return console.log(error)
        }
        forecast.forecast(latitude, longitude, (error, { temperature, weather }) => {
            if (error) {
                return console.log(error)
            }
            console.log(location)
            console.log('Temperature: ' + temperature + ' degree celcius. It is ' + weather)
        })
    })
}